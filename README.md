# The Incident Report project is used to integrate the chatbot for servicenow using dialogflow.

Used to create and view incidents in servicenow.

```
npm install

npm start

```

Open Postman.

POST http://localhost:3000/botHandler/

POST  https://c55c2b77.ngrok.io/botHandler

{
  "responseId": "0ac00fc8-570a-4cdc-9d73-a67be0b6c5fd-68e175c7",
  "queryResult": {
    "queryText": "Create a Ticket with description1, comment1, category1",
    "parameters": {
      "description": "description1",
      "comment": "comment1",
      "category": "category1"
    },
    "allRequiredParamsPresent": true,
    "fulfillmentMessages": [
      {
        "text": {
          "text": [
            ""
          ]
        }
      }
    ],
    "intent": {
      "name": "projects/servicenow-local-ibnjhw/agent/intents/5d2947c4-1012-4917-850e-07d02b8156db",
      "displayName": "CreateIncident"
    },
    "intentDetectionConfidence": 1,
    "languageCode": "en"
  },
  "originalDetectIntentRequest": {
    "payload": {}
  },
  "session": "projects/servicenow-local-ibnjhw/agent/sessions/fde7c70e-2700-3344-f0ca-cf281d46c678"
}


GET https://dev71283.service-now.com/api/377308/getincidentdetails/INC0000058

Then test against locally running nodejs server:

POST http://localhost:3000/botHandler/

POST https://c55c2b77.ngrok.io/botHandler
{
  "responseId": "8e89296b-c0c7-421d-9304-7e212586ecd4-68e175c7",
  "queryResult": {
    "queryText": "please check the state of INC0000050",
    "parameters": {
      "any": "INC0000050"
    },
    "allRequiredParamsPresent": true,
    "fulfillmentMessages": [
      {
        "text": {
          "text": [
            ""
          ]
        }
      }
    ],
    "intent": {
      "name": "projects/servicenow-local-ibnjhw/agent/intents/44283031-39c4-4df4-81dd-aa94e2f03643",
      "displayName": "GetIncident"
    },
    "intentDetectionConfidence": 1,
    "languageCode": "en"
  },
  "originalDetectIntentRequest": {
    "payload": {}
  },
  "session": "projects/servicenow-local-ibnjhw/agent/sessions/4fa510aa-839c-dba8-56d1-652e911963e2"
}

Login to DialogFlow, make sure Fulfillment webhook points to:

https://f8879b47.ngrok.io/botHandler/

and test the CreateIncident with:

Create a Ticket with description1, comment1, category1

and the GetIncident with:

please check the state of INC0000050


Heroku

heroku.com - install heroku cli

heroku -v
heroku login
heroku create
heroku rename gordonfrog2
https://gordonfrog2.herokuapp.com
https://git.heroku.com/gordonfrog2.git
git remote add heroku https://git.heroku.com/gordonfrog2.git
git push heroku master
heroku logs --tail
https://gordonfrog2.herokuapp.com/

Open Postman:

https://gordonfrog2.herokuapp.com/botHandler
{
  "responseId": "8e89296b-c0c7-421d-9304-7e212586ecd4-68e175c7",
  "queryResult": {
    "queryText": "please check the state of INC0010046",
    "parameters": {
      "any": "INC0010046"
    },
    "allRequiredParamsPresent": true,
    "fulfillmentMessages": [
      {
        "text": {
          "text": [
            ""
          ]
        }
      }
    ],
    "intent": {
      "name": "projects/servicenow-local-ibnjhw/agent/intents/44283031-39c4-4df4-81dd-aa94e2f03643",
      "displayName": "GetIncident"
    },
    "intentDetectionConfidence": 1,
    "languageCode": "en"
  },
  "originalDetectIntentRequest": {
    "payload": {}
  },
  "session": "projects/servicenow-local-ibnjhw/agent/sessions/4fa510aa-839c-dba8-56d1-652e911963e2"
}

Change Fulfillment Webhook in DialogFlow to https://gordonfrog2.herokuapp.com/botHandler and test!