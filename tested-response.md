# response

{
  "speech": "Reqeuest Confirmation",
  "displayText": "Reqeuest Confirmation",
  "message": {
    "platform": "servicenow",
    "speech": "Hi. I am your ServiceNow Assistant. I can help you open a ticket or view an existing ticket.  Please either Post a 'view request' or a 'create request'."
  },
  "result": {
    "platform": "servicenow",
    "action": "reportIncident"
  },
  "view request": {
    "platform": "servicenow",
    "action": "getIncident",
    "Number": "INC0000058"
  },
  "create request": {
    "platform": "servicenow",
    "action": "reportIncident",
    "Number": "INC0000058",
    "State": "Texas",
    "Short Description": "Having an issue with my username.",
    "Assignment Group": "Tech Support"
  }
}