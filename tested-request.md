# request

{
  "responseId": "db02a8f1-6f57-4450-ba32-ac7b8516d460-68e175c7",
  "queryResult": {
    "queryText": "Create a Ticket with description1, comment1, category1",
    "parameters": {
      "category": "category1",
      "description": "description1",
      "comment": "comment1"
    },
    "allRequiredParamsPresent": true,
    "fulfillmentMessages": [
      {
        "text": {
          "text": [
            ""
          ]
        }
      }
    ],
    "intent": {
      "name": "projects/servicenow-local-ibnjhw/agent/intents/5d2947c4-1012-4917-850e-07d02b8156db",
      "displayName": "CreateIncident"
    },
    "intentDetectionConfidence": 1,
    "diagnosticInfo": {
      "webhook_latency_ms": 185
    },
    "languageCode": "en"
  },
  "webhookStatus": {
    "message": "Webhook execution successful"
  }
}
