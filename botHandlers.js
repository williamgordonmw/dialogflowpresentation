var DialogflowApp	=	require('actions-on-google').DialogflowApp;
var ServiceNow = require('./serviceNow');
var request			=	require('request');
var botHandlers = {};
//var botResponses = require('./facebook.js');
botHandlers.processRequest = function(req, res){
	return new Promise(function(resolve, reject){
		console.log('Process request started');
		if (typeof req.body.result === 'undefined' || typeof req.body.result.action === 'undefined') {
			console.log('Initializing for intents');
			if (req.body.queryResult) {
				console.log('queryText: '+ req.body.queryResult.queryText);
				if (!req.body.result) {
					console.log('initializing result');
					req.body.result = {};
				}
	
				if (req.body.queryResult.intent && req.body.queryResult.intent.displayName && req.body.queryResult.intent.displayName == 'CreateIncident') {
					console.log('CreateIncident');
					console.log('category parameter - '+ req.body.queryResult.parameters.category);
					console.log('description parameter - '+ req.body.queryResult.parameters.description);
					console.log('comment parameter - '+ req.body.queryResult.parameters.comment);
					req.body.result.short_description = req.body.queryResult.parameters.description;
					req.body.result.comments = req.body.queryResult.parameters.comment;
					req.body.result.category = req.body.queryResult.parameters.category;
					req.body.result.urgencyType = 'user';
					req.body.result.contactDetails = 'new details'
					req.body.result.action = 'reportIncident';
				}
				if (req.body.queryResult.intent && req.body.queryResult.intent.displayName && req.body.queryResult.intent.displayName == 'GetIncident') {
					console.log('GetIncident');
					console.log('any (incident id) parameter - '+ req.body.queryResult.parameters.any);
	
					req.body.result.incidentId = req.body.queryResult.parameters.any;
					req.body.result.action = 'getIncident';
				}
				
			}
		} 
		let action = req.body.result.action; // https://dialogflow.com/docs/actions-and-parameters		
		console.log('1. action: '+action);
		let parameter = req.body.queryResult.parameters ? req.body.queryResult.parameters.any : undefined; // https://dialogflow.com/docs/actions-and-parameters		
		console.log('2. parameter: '+parameter);
		let inputContexts = req.body.queryResult.contexts ? req.body.result.contexts : undefined; // https://dialogflow.com/docs/contexts
		console.log('3. inputContexts: '+inputContexts);
		
		var sessionId = req.body.session;		
		console.log('sessionId: '+sessionId);
		
		if(action == 'getIncident'){
			//return trackIncident(parameter,sessionId);
			ServiceNow.getIncidentDetails(res, parameter.trim())
            .then(success => {
                console.log(JSON.stringify(success))
                //return createIncident(state, description, success.result[0].name)
                
                let message = '';
                if (success == '') {
                     message = 'There is no incident found with the given incident Id';
                    
                     console.log("Bot Failed - " + message);
                     console.log("Bot Failed - " + JSON.stringify((message)));
                     return res.json((message));
                    
                } else {
                    console.log("Bot Succeeded - " + JSON.stringify((success.body)));
                    return res.json((createGetIncidentResponse(success)));
                }
            }).catch((error) => {
                console.log(error);
                let message = "Cannot get the incident details. Try again later";
                
                console.log("Bot - " + JSON.stringify((message)));
                return JSON.stringify((message)); 
            });
		}
		else {
			ServiceNow.saveIncident(res, req.body.result).then(response => {
				let message = ' Your incident is noted. We will let you know after completing. Please note this Id - ' + response.number + ' for further reference ';
				console.log("Bot - " + JSON.stringify((message)));
				return res.json((createPostIncidentResponse(response)));
			}).catch((error) => {
				let message = 'Unable to save the incident. Try again later';
				
				console.log("Bot - " + JSON.stringify((message)));
				return JSON.stringify((message));
			});
		}
	})
}

function createGetIncidentResponse(incidentResponse) {
    let incidentJson = JSON.stringify(incidentResponse.body).replace("\\","");
    return {
        "fulfillmentText": incidentJson,
    }
}

function createPostIncidentResponse(text) {
    var fulfillmentText = text.number + ' has been created.';
    return {
        //"fulfillmentText": text
        "fulfillmentText": fulfillmentText
    }
}

function createIncident(sessId){
	console.log('creation started',incidentTickets[sessId]);		
	return new Promise(function(resolve,reject){
		var options = { 
			method: 'POST',
			url: 'https://dev18442.service-now.com/api/now/v1/table/incident',
			headers:{ 
				'postman-token': 'd6253bf3-ff31-fb21-7741-3dd02c84e8bb',
				'cache-control': 'no-cache',
				authorization: 'Basic MzMyMzg6YWJjMTIz',
				'content-type': 'application/json' 
			},
			body:{ 
				short_description	: 	'testing incident',
				caller_id			: 	'TST',
				Caller				:	incidentTickets[sessId].caller,
				urgency				: 	incidentTickets[sessId].urgency,
				state				:	incidentTickets[sessId].state,
				incident_state		:	incidentTickets[sessId].incidentState,
				category			:	incidentTickets[sessId].category,
				subcategory			:	incidentTickets[sessId].subCategory,
				//workingGroup		:	incidentTickets[sessId].workingGroup,
				impact				:	incidentTickets[sessId].impact,
				priority			:	incidentTickets[sessId].priority,
				contact_type		:	incidentTickets[sessId].contactType,
				comments			: 	'Chatbot Testing',
				Assigned_to			:	incidentTickets[sessId].assignedTo		
			},			
			json: true 
		}; 
		delete incidentTickets[sessId];		
		request(options, function (error, response, body) {
			var rsp = {  
					"speech":"",
					"displayText":"",
					"data":{  
						"facebook":{  
							"text":	""
						}
					}
				}
			if (error) {
				rsp.data.facebook.text = JSON.stringify(error);
			}else{			
				rsp.data.facebook.text = "Incident Created Ur Incident Number \n"+body.result.number+"\n please Note for future reference" 	
			}
			resolve(rsp);
		});
		
	})
}
function trackIncident(incNum, sessId){
	return new Promise(function(resolve,reject){
		console.log('tracking started: ' + incNum);		
		// var fstr = incNum.substring(0,3);
		// var sstr = incNum.substring(3);
		var rsp = {  
					"speech":"",
					"displayText":"",
					"data":{  
						"facebook":{  
							"text":	""
						}
					}
				}
		// 		console.log(fstr == 'inc'&&!isNaN(sstr));
		//if(fstr == 'inc'&&!isNaN(sstr)){
			var options = { 
				method: 'GET',
				//url: 'https://dev71283.service-now.com/api/now/v1/table/incident',
				url: 'https://dev71283.service-now.com/api/377308/getincidentdetails/',
				qs: { 
					//number: incNum.toUpperCase()
					number: incNum
				},
				headers:{
					'postman-token': '5441f224-d11a-2f78-69cd-51e58e2fbdb6',
					'cache-control': 'no-cache',
					authorization: 'Basic MzMyMzg6YWJjMTIz' 
				},json: true  
			};
			request(options, function (error, response, body) {
				if (error) {
					console.log(JSON.stringify(error));	
					rsp.data.facebook.text = "Incident not exist : "+JSON.stringify(error);
				}else{			
					
					if(body.error){
						console.log(JSON.stringify(body.error));	
						rsp.data.facebook.text = "incident not exist\n please enter valid incident";
					}else{
						console.log('success');	
						rsp.data.facebook.text = "incident exist : Incident updated on : "+body.result[0].sys_updated_on;
					}
					
					
				}
				resolve(rsp);
			});
			//delete incidentTickets[sessId];
		// }else{
		// 	rsp.data.facebook.text = "Please enter valid incident number";
		// 	resolve(rsp);
		// }
		
	});
}

module.exports = botHandlers;