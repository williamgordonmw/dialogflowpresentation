# This is a Facebook Bot.
## This bot uses dialogflow AI engine.

# Description:
### It is a simple silly name maker, which asks you some questions and based on your answers, it creates your silly name.

### Tech

This code uses a number of open source projects to work properly. The following tools were used:

- [node.js](https://nodejs.org/) - evented I/O for the backend
- [Express](https://www.npmjs.com/package/express) - fast node.js network app framework
- [winston](https://www.npmjs.com/package/winston) - A multi-transport async logging library for node.js.
- [MySQL](https://www.npmjs.com/package/mysql) - A relational database management system (RDBMS) for storing data and 
retreiving data. 
- [apiai](https://www.npmjs.com/package/apiai) - This plugin allows integrating agents from the Api.ai natural language processing service with your Node.js application.


curl -d "grant_type=refresh_token&client_id=99109370a3a23300f172f1e4bac7f0d5&client_secret=secret&refresh_token=m2qUXBcQFNxqxOK7NUl-BOqw-m2hgVvcOVnOJtopBycSfzmOy9WxwbHdcEkjp2ucxq9IgEpq7Y_9N7mh7DAuew" https://dev71283.service-now.com/oauth_token.do

Grab NwhsAN2fUbn5W4X16oqXxoEhFNA6M6Sfd8cLosA9-DqaM6wg_x9iZNVSRU6QCI-VF4tgLBO4Ji0d3frbEUrC5A

Insert Token in processMessage

## Installation

It requires Node.js v6+ to run.
Install the dependencies and devDependencies and start the server.

```sh
$ cd facebookbot-apiai
$ npm install
$ npm start
```