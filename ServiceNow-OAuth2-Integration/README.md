# ServiceNow-OAuth2-Integration
Sample example for authenticating ServiceNow REST API through OAuth2.0

1. Create an OAuth endpoint for you application in ServiceNow. Please take a note of ClientID and ClientSecret.

https://developer.servicenow.com/app.do#!/document/content/app_store_doc_rest_integrate_london_t_EnableOAuthWithREST?v=london

Basic Auth Test first in Postman:

GET https://dev71283.service-now.com/api/now/table/incident

401 - Unauthorized

add: Authorization:  admin / M___3

Authorization Header: Basic (admin / M___3)Base64-Encoded

curl "https://dev71283.service-now.com/api/now/table/incident" -H "Accept:application/json" --user 'admin':'M____3'

200 - OK.

OAUTH Test in Postman:

GET https://dev71283.service-now.com/api/now/table/incident

401 - Unauthorized

add: Authorization:  Bearer: token_here

Authorization Header: Bearer Token

Login to ServiceNow as Admin and elevate role to security admin.

Search OAUTH, Create an OAuth API endpoint for external clients

Name: oauth_endpoint

Client Secret: secret

Client ID: 99109370a3a23300f172f1e4bac7f0d5

30min or 1,800sec

Refresh is 100days

Endpoint URL: https://dev71283.service-now.com/oauth_token.do

Body: x-wwww-form-urlencoded

grant_type: password
client_id: 99109370a3a23300f172f1e4bac7f0d5
client_secret: secret
username: admin
password: M___3

200 - OK.

{
    "access_token": "gokXjTGN47TlLf4x3Ax1kW6sdYHWd8jxc5XikBCGkkPA5vSdHtX4xjxKrYcxVVR6r884BH9tSghrYTknjL_5KA",
    "refresh_token": "m2qUXBcQFNxqxOK7NUl-BOqw-m2hgVvcOVnOJtopBycSfzmOy9WxwbHdcEkjp2ucxq9IgEpq7Y_9N7mh7DAuew",
    "scope": "useraccount",
    "token_type": "Bearer",
    "expires_in": 1799
}

From Postman set:

Accept: application/json
Authorization: Bearer gokXjTGN47TlLf4x3Ax1kW6sdYHWd8jxc5XikBCGkkPA5vSdHtX4xjxKrYcxVVR6r884BH9tSghrYTknjL_5KA

curl "https://dev71283.service-now.com/api/now/table/incident" -H "Accept:application/json" -H "Authorization:Bearer gokXjTGN47TlLf4x3Ax1kW6sdYHWd8jxc5XikBCGkkPA5vSdHtX4xjxKrYcxVVR6r884BH9tSghrYTknjL_5KA" 



Refresh Token:

From Postman set:

Endpoint URL: https://dev71283.service-now.com/oauth_token.do

Body: x-wwww-form-urlencoded

grant_type: refresh_token
client_id: 99109370a3a23300f172f1e4bac7f0d5
client_secret: secret
refresh_token: m2qUXBcQFNxqxOK7NUl-BOqw-m2hgVvcOVnOJtopBycSfzmOy9WxwbHdcEkjp2ucxq9IgEpq7Y_9N7mh7DAuew

curl -d "grant_type=refresh_token&client_id=99109370a3a23300f172f1e4bac7f0d5&client_secret=secret&refresh_token=m2qUXBcQFNxqxOK7NUl-BOqw-m2hgVvcOVnOJtopBycSfzmOy9WxwbHdcEkjp2ucxq9IgEpq7Y_9N7mh7DAuew" https://dev71283.service-now.com/oauth_token.do

{
    "access_token": "gokXjTGN47TlLf4x3Ax1kW6sdYHWd8jxc5XikBCGkkPA5vSdHtX4xjxKrYcxVVR6r884BH9tSghrYTknjL_5KA",
    "refresh_token": "m2qUXBcQFNxqxOK7NUl-BOqw-m2hgVvcOVnOJtopBycSfzmOy9WxwbHdcEkjp2ucxq9IgEpq7Y_9N7mh7DAuew",
    "scope": "useraccount",
    "token_type": "Bearer",
    "expires_in": 1799
}



2. Now update config file with ClientID and ClientSecret.


3. Run `npm run server` to start backend server.

