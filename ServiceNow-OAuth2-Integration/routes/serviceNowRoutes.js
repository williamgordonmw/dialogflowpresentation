const express = require('express');
const router = express.Router();
const axios = require('axios');

router.get('/incidents/sample',(req,res)=>{
    const options={
        //url:`https://dev71283.service-now.com/api/now/table/incident?sysparm_limit=1`,
        url:`https://dev71283.service-now.com/api/now/table/incident`,
        method:'get',
        // headers:{
        //     //'Authorization':`Bearer ${req.user}`

        //     'Authorization':`Bearer Hjti0Zij-JkRTTQqmGVldVAJW66fa1xqfKJxSCHY45HSV794sXi8v9HEIcppqNKZMmUyLma9Xfd5mVPyvgMCng`
        // }
        headers: { 'Authorization': 'Basic YWRtaW46TUB2M24xMjM=' }
    };
    axios(options).then((val)=>{
        res.send(val.data.result);
    }).catch((err)=>{
        res.send(err);
    });
});

module.exports = router;
